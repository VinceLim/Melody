/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Auteur : Vincent Limorté
 * Mars 2017
 */

#ifndef _MELODY_H
#define _MELODY_H

#include "pitch.h"
#include "Arduino.h"

#define MELODY_VERSION "v0.0.1"



void handlePlayerInterrupt();

class Melody {
public:
	/**
	 * la note est jouée duree*tempo ms
	 */
	Melody(unsigned int* notes, long* duree, int nombre, int tempo);
	unsigned int* notes;
	long* durees;
	int nombre;
	int tempo;
	~Melody();

};



class Player {
public:
	Player(uint8_t pin);
	void play(Melody &m);
	void play(unsigned int note, unsigned long duration);
	void stop();
	bool isPlaying();
	bool forceStop();
	~Player();
private:
	uint8_t pin;
	bool playing;
	volatile bool _forceStop;
};

// active player for interruption callback
extern Player* activePlayer;
extern bool activePlayerSet;

#endif //_MELODY_H
