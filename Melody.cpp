/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Auteur : Vincent Limorté
 * Mars 2017
 */
#include "Melody.h"

bool activePlayerSet;
Player* activePlayer;

Melody::Melody(unsigned int* notes, long* duree, int nombre, int tempo) {
	this->notes = notes;
	this->durees = duree;
	this->nombre = nombre;
	this->tempo = tempo;
}

Melody::~Melody() {
}

Player::Player(uint8_t pin) {
	this->pin = pin;
	playing=false;
	pinMode(pin, OUTPUT);
}

void handlePlayerInterrupt(){
	detachInterrupt(0);
	activePlayer->forceStop();
	activePlayer=NULL;
	activePlayerSet=false;
}

void Player::play(Melody &m) {
	// Stop whoever is playing
	if(activePlayer != NULL){
		activePlayer->forceStop();
	}

	// Prepare interrupt
	activePlayer = this;
	activePlayerSet = true;
	/* Setup pin2 as an interrupt and attach handler. */
	attachInterrupt(0, handlePlayerInterrupt, LOW);
	_forceStop = false;

	playing=true;
	for(int i=0;i<m.nombre && !_forceStop ;i++){

		if (m.notes[i] != NOTE_NONE) {
			tone(pin, m.notes[i]);
		}
		delay( abs(m.durees[i]) * m.tempo);

		noTone(pin);
		// pause between notes
		if(m.durees[i]>0){
			delay(m.tempo / 2);
		}
	}
	if(activePlayerSet){
		activePlayer=NULL;
		activePlayerSet=false;
		detachInterrupt(0);
	}
	stop();
}

bool Player::forceStop(){
	 return _forceStop=true;
}

void Player::stop() {
	noTone(pin);
	playing=false;
}

bool Player::isPlaying(){
	return playing;
}

void Player::play(unsigned int note, unsigned long duration) {
	playing=true;
	tone(pin, note, duration);
	delay(duration);
	stop();
}

Player::~Player() {
	if(activePlayerSet){
			activePlayer=NULL;
			activePlayerSet=false;
			detachInterrupt(0);
		}
	stop();
}




